#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/8/12 下午12:00
# @Author  : 冷月孤心
# @Mail    : codenutter@foxmail.com
# @File    : wechat_alert.py


import requests
import random
import logging
import json
import sys
import redis
import warnings

from logger import init_logger
"""
参数	     是否必须	     说明
touser	  否	       成员ID列表（消息接收者，多个接收者用‘|’分隔，最多支持1000个）。特殊情况：指定为@all，则向该企业应用的全部成员发送
toparty	  否	       部门ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
totag	  否	       标签ID列表，多个接收者用‘|’分隔，最多支持100个。当touser为@all时忽略本参数
msgtype	是	       消息类型，此时固定为：text
agentid	是	       企业应用的id，整型。可在应用的设置页面查看
content	是	       消息内容，最长不超过2048个字节
safe	否	       表示是否是保密消息，0表示否，1表示是，默认0
"""
reload(sys)
sys.setdefaultencoding('utf-8')
warnings.filterwarnings("ignore")  # 去掉所有运行时警告输出
Redis_HOST = "127.0.0.1"
Redis_PORT = 6379

def set_redis_value(key, value, extime):
    pool = redis.ConnectionPool(host = Redis_HOST, port = Redis_PORT)
    r = redis.Redis(connection_pool=pool)
    r.set(key, value, extime)


def get_redis_value(key):
    pool = redis.ConnectionPool(host=Redis_HOST, port=Redis_PORT)
    r = redis.Redis(connection_pool=pool)
    return r.get(key)


def get_token(corpid, secret):
    """
    获取根据企业corpid和应用secret获取access_token
    :param corpid: 企业corpid
    :param secret: 应用secret
    :return: 获取成功返回access_token，否则返回None
    """
    init_logger('wechat_send.log', logging.INFO, '%(asctime)s|%(levelname)s|%(message)s', '%Y-%m-%d %H:%M:%S')

    # 先从redis请求token
    access_token = get_redis_value('access_token')
    if access_token:
        logging.info("Token OK|From redis|%s" % access_token)
        return access_token

    # reids没有token则发送http请求token，并重新缓存
    # 构建获取token的url
    get_token_url = 'https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=%s&corpsecret=%s' % (corpid, secret)
    USER_AGENTS = (
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:11.0) Gecko/20100101 Firefox/11.0',
            'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:22.0) Gecko/20100 101 Firefox/22.0',
            'Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0'
        )
    try:
        response = requests.get(get_token_url, headers={'User-Agent': random.choice(USER_AGENTS)}, verify=False)
    except Exception as e:
        logging.error("Token Error|From http|%s" % e.message)
        return None

    token_json = response.json()
    if 'access_token' in token_json:
        logging.info("Token OK|From http|%s" % response.text)
        access_token = token_json['access_token']
        set_redis_value('access_token', access_token, 7100)  # 微信token默认7200s后失效，这里缓存7100s
        return access_token
    else:
        logging.error("Token Error|From http|%s" % response.text)
        return None


def wechat_alert(touser, toparty, totag, agentid, content, access_token):
    """
    微信发送接口
    :param touser: 用户微信号
    :param toparty: 部门ID
    :param totag: 标签列表
    :param agentid: 应用ID
    :param content: 消息内容，最长不超过2048个字节
    :return:
    """
    request_url = 'https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=%s' %(access_token)
    headers = {'content-type': 'application/json'}
    if len(content) > 2048:
        content = content[:2045] + "..."
    payload = {
        "touser": touser and touser or '',
        "toparty": toparty and toparty or '',
        "totag": totag and totag or '',
        "msgtype": "text",
        "agentid": agentid,
        "text": {
            "content": content.encode('UTF-8')
        },
        "safe": "0"
    }

    try:
        response = requests.post(request_url, data=json.dumps(payload, ensure_ascii=False), headers=headers, verify=False)
        logging.info("Send OK|%s|%s|%s|%s" % (touser, toparty, content, response.text))
    except Exception as e:
        logging.error("Send Error|%s|%s|%s|%s" % (touser, toparty, content, e.message))


if __name__ == "__main__":
    #  使用方法：python wechat.py "接收者微信号" "消息内容"
    corpid = "xxxxxxxxxxx"
    secret = "xxxxxxxxxxx"

    touser= str(sys.argv[1])
    toparty = "2"
    totag = ''
    agentid = 1000003
    content = str(sys.argv[2])
    init_logger('wechat_send.log', logging.INFO, '%(asctime)s|%(levelname)s|%(message)s', '%Y-%m-%d %H:%M:%S')

    access_token = get_token(corpid, secret)
    if access_token:
        wechat_alert(touser, toparty, totag, agentid, content, access_token)
