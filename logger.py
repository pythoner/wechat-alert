#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2017/7/15 下午9:48
# @Author  : 冷月孤独心
# @Mail    : haohao.qiang@renren-inc.com
# @File    : logger.py

import logging
""" -------  时间格式化参数   ---------
        %a	Locale’s abbreviated weekday name.
        %A	Locale’s full weekday name.
        %b	Locale’s abbreviated month name.
        %B	Locale’s full month name.
        %c	Locale’s appropriate date and time representation.
        %d	Day of the month as a decimal number [01,31].
        %H	Hour (24-hour clock) as a decimal number [00,23].
        %I	Hour (12-hour clock) as a decimal number [01,12].
        %j	Day of the year as a decimal number [001,366].
        %m	Month as a decimal number [01,12].
        %M	Minute as a decimal number [00,59].
        %p	Locale’s equivalent of either AM or PM.	(1)
        %S	Second as a decimal number [00,61].	(2)
        %U	Week number of the year (Sunday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Sunday are considered to be in week 0.	(3)
        %w	Weekday as a decimal number [0(Sunday),6].
        %W	Week number of the year (Monday as the first day of the week) as a decimal number [00,53]. All days in a new year preceding the first Monday are considered to be in week 0.	(3)
        %x	Locale’s appropriate date representation.
        %X	Locale’s appropriate time representation.
        %y	Year without century as a decimal number [00,99].
        %Y	Year with century as a decimal number.
        %Z	Time zone name (no characters if no time zone exists).
        %%	A literal '%' character.
    """

def init_logger(filename, level, format, datefmt):
    """
    初始化日志记录器
    :param filename: 日志文件名
    :param level: 日志级别
    :param format: 格式化输出
    :param datefmt: 日志日期格式
    :return: None
    """
    logging.basicConfig(filename=filename, level=level, format=format, datefmt=datefmt)